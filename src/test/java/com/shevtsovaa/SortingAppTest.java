package com.shevtsovaa;

import static org.junit.Assert.assertEquals;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class SortingAppTest {
    /**
     *Test class. We check if the length of array in between 2 and 10 elements inclusive.
     */
    BubbleSort bs = new BubbleSort();

    private final int[] arrayTest;
    private final String expected;

    public SortingAppTest(int[] arrayTest, String expected) {
        this.arrayTest = arrayTest;
        this.expected = expected;
    }

    @Test(expected = IllegalArgumentException.class)
    @DisplayName("Test with zero elements.")
    public void testZeroElementsCase() {
        int[] zeroElementsArray = new int[0];
        bs.bubbleSort(zeroElementsArray);
    }

    @Test(expected = IllegalArgumentException.class)
    @DisplayName("Test with one element.")
    public void testOneElementCase() {
        int[] oneElementArray = new int[1];
        bs.bubbleSort(oneElementArray);
    }

    @Test(expected = IllegalArgumentException.class)
    @DisplayName("Test with more than ten elements.")
    public void testMoreThanTenElementsCase() {
        int[] moreThanOneElementArray = new int[11];
        bs.bubbleSort(moreThanOneElementArray);
    }

    @Test
    @DisplayName("Test with enable elements quantity.")
    public void testEnableElementsQuantityCase() {
        assertEquals(expected, bs.bubbleSort(arrayTest));
    }

    @Contract(pure = true)
    @Parameterized.Parameters
    public static @NotNull Collection <Object[]> input() {
        int[] array1 = {32, -63, 2, 36, 32, -41, 0, 1, 3, 69},
                array2 = {6, -1, 0, 9, 3},
                array3 = {3, 9, 1};
        return Arrays.asList(new Object[][]{
                {array1, "[-63, -41, 0, 1, 2, 3, 32, 32, 36, 69]"},
                {array2, "[-1, 0, 3, 6, 9]"},
                {array3, "[1, 3, 9]"},
        });
    }
}