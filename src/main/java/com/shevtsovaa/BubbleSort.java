package com.shevtsovaa;

import java.util.Arrays;

public class BubbleSort {
    /**
     * Here we check a quantity of elements we got and sort array of int numbers
     * in ascending order using bubble sort algorithm.
     * @param sortArr unsorted array from main class.
     * @return string with sorted array.
     */
    public String bubbleSort(int[] sortArr) {

        if (sortArr.length == 0) throw new IllegalArgumentException("0 elements passed");
        if (sortArr.length == 1) throw new IllegalArgumentException("only 1 element passed");
        if (sortArr.length > 10) throw new IllegalArgumentException("more than 10 elements passed");

        for (int i = 0; i < sortArr.length - 1; i++) {
            for (int j = 0; j < sortArr.length - i - 1; j++)
                if (sortArr[j + 1] < sortArr[j]) {
                    int swap = sortArr[j];
                    sortArr[j] = sortArr[j + 1];
                    sortArr[j + 1] = swap;
                }
        }
        return Arrays.toString(sortArr);
    }
}

