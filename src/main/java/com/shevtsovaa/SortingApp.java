package com.shevtsovaa;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * @author Andrei Shevtsov.
 * @version 1.0
 *This program is forming aa array of integers from console input and sort
 * this array in ascending order using a bubble sort method.
 */

public class SortingApp {
    private static final Logger logger = LoggerFactory.getLogger(SortingApp.class);
    /**
     *In this main method we fulfill the array in random order from one
     * to ten elements and filter input from any other symbols except +, - and numbers
     * @param args from console.
     */

    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();
        int counter = 0;
        int maxLimitOfElements = 10;
        String enteredValue;
        String regex = "[+-]?\\d+";

        System.out.println("Input the whole number positive or negative" +
                " and press Enter button. Or just press Enter to exit. (max 10 pcs):");
        Scanner sc = new Scanner(System.in);

        do {
            enteredValue = sc.nextLine();
            if (enteredValue.matches(regex)) {
            int element = Integer.parseInt (enteredValue);
            list.add(element);
            counter++;}
            else if (enteredValue.isEmpty()) break;
            else System.out.println("Your input wasn't a number, please try again:");
        }
        while (counter < maxLimitOfElements);


        logger.warn("Let's sort this array!");
        int[] array = list.stream().mapToInt(i -> i).toArray();

        logger.warn("Unsorted array:" + Arrays.toString(array));
        BubbleSort bs = new BubbleSort();
        try {
            if (array != null) bs.bubbleSort(array);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            throw new IllegalArgumentException(ex);
        }

        logger.warn("Sorted array:" + Arrays.toString(array));
    }
}
